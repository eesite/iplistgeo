#!/usr/bin/env python3

import argparse
import sys
import csv

IPV4_BLOCKS = "GeoLite2-Country-Blocks-IPv4.csv"
IPV6_BLOCKS = "GeoLite2-Country-Blocks-IPv6.csv"
COUNTRY_LOCATIONS = "GeoLite2-Country-Locations-en.csv"

parser = argparse.ArgumentParser(description=\
            """Generate lists of IP addresses from an GeoLite2 Country
                    database in CSV format""")

parser.add_argument("-d", "--directory", \
                    help="GeoLite2 directory", required=True)

parser.add_argument("-c", "--countries", \
                    help="countries, comma separated")

parser.add_argument("-r", "--registered", \
                    help="registered countries, comma separated")
                    
parser.add_argument("-p", "--represented", \
                    help="represented countries, comma separated")

parser.add_argument("-s", "--satellite", \
                    help="select only satellite provider addresses", \
                    choices=['y', 'n'])
                    
parser.add_argument("-x", "--proxy", \
                    help="select only anonymous proxy addresses", \
                    choices=['y', 'n'])

args = parser.parse_args()

# build the <geoname_id> lists
locations_file = open(args.directory + '/' + COUNTRY_LOCATIONS, 'r', \
                        encoding='utf-8')
locations_reader = csv.reader(locations_file, delimiter=',')

# ip list filename is c_<countries>_r<countries>_p_<countries>_s<y/n>...
list_filename = ''

country_ids = []

if args.countries:    
    country_list = args.countries.upper().split(',')
    list_filename = list_filename + 'c' + '_'.join(country_list)
    
    for row in locations_reader:
        if (row[4] in country_list) or \
                (row[4] == '' and row[2] in country_list):
            country_ids.append(row[0])

registered_ids = []

if args.registered:
    country_list = args.registered.upper().split(',')
    
    if len(list_filename) > 0:
        list_filename = list_filename + '_'

    list_filename = list_filename + 'r' + '_'.join(country_list)

    for row in locations_reader:
        if (row[4] in country_list) or \
                (row[4] == '' and row[2] in country_list):
            registered_ids.append(row[0])

represented_ids = []
    
if args.represented:
    country_list = args.represented.upper().split(',')

    if len(list_filename) > 0:
        list_filename = list_filename + '_'

    list_filename = list_filename + 'p' + '_'.join(country_list)

    for row in locations_reader:
        if (row[4] in country_list) or \
                (row[4] == '' and row[2] in country_list):
            represented_ids.append(row[0])

locations_file.close()

if args.proxy:
    if len(list_filename) > 0:
        list_filename = list_filename + '_'
    list_filename = list_filename + 'x' + args.proxy

if args.satellite:
    if len(list_filename) > 0:
        list_filename = list_filename + '_'
    list_filename = list_filename + 's' + args.satellite


def write_ip_list(geo_csv_filename, ip_list_filename):
    geo_csv_file = open(geo_csv_filename, 'r')
    geo_csv_reader = csv.reader(geo_csv_file, delimiter=',')
    ip_list_file = open(ip_list_filename, 'w')

    for row in geo_csv_reader:
        ip_block = row[0]
        append_block = True

        if args.countries:
            if row[1] not in country_ids:
                append_block = False

        if args.registered and append_block:
            if row[2] not in registered_ids:
                append_block = False

        if args.represented and append_block:
            if row[3] not in represented_ids:
                append_block = False

        if args.proxy and append_block:
            if args.proxy == 'n' and row[4] != '0':
                append_block = False
            elif args.proxy == 'y' and row[4] != '1':
                append_block = False

        if args.satellite and append_block:
            if args.satellite == 'n' and row[5] != '0':
                append_block = False
            elif args.satellite == 'y' and row[5] != '1':
                append_block = False

        if append_block:
            print(ip_block, file=ip_list_file)

    geo_csv_file.close()
    ip_list_file.close()


# find IPv4 blocks
write_ip_list(args.directory + '/' + IPV4_BLOCKS, \
                list_filename + '.ip4')

# find IPv6 blocks
write_ip_list(args.directory + '/' + IPV6_BLOCKS, \
                list_filename + '.ip6')
