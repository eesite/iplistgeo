# iplistgeo


## Description

Those are two Python 3 scripts that will extract a lists of IPv4 and IPv6 addresses, from a GeoLite2 Country 
database in CSV format, or from a GeoLite2 ASN database in CSV format, according to the options provided.

The files generated can be used in firewall rules, for example with the pf firewall in *BSD or OS X.


## Requirements

* Python 3
* [GeoLite2 data created by MaxMind - GeoLite2 Country database in CSV format, available from http://www.maxmind.com](http://www.maxmind.com)
* [GeoLite2 data created by MaxMind - GeoLite2 ASN database in CSV format, available from http://www.maxmind.com](http://www.maxmind.com)


## Usage


### iplistgeo

1. Get the [GeoLite2 Country database in CSV format](http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country-CSV.zip)
	and extract the archive
2. Run the script `./iplistgeo.py -d <Geolite2-directory> -r <country-list> -s <y/n> -x <y/n>`


#### Options

* `-c <country-list>` -- countries
* `-r <country-list>` -- registered countries
* `-p <country-list>` -- represented countries
* `-s <y/n>` -- include satellite provider addresses
* `-x <y/n>` -- include anonymous proxy addresses

<country-list> is a comma separated two letter ISO country code


#### Examples

```
./iplistgeo.py -d GeoLite2-Country-CSV_20180403 -c RO,BG,CN -s n -x n
```

Writes the ip list files: `cRO_BG_CN_xn_sn.ip4` and `cRO_BG_CN_xn_sn.ip6`.


### iplistasn

1. Get the [GeoLite2 ASN database in CSV format](http://geolite.maxmind.com/download/geoip/database/GeoLite2-ASN-CSV.zip)
	and extract the archive
2. Run the script `./iplistasn.py -d <Geolite2-asn-directory> -n <ASNs> -c <companies>`


#### Options

* `-n <ASNs>` -- comma separated list of ASN numbers
* `-c <companies>` -- company name, or comma separated list


#### Examples

```
./iplistasn.py -d GeoLite2-ASN-CSV_20180403 -n 12222
```

Writes the ip list files: `ASN_12222.ip4` and `ASN_12222.ip6`.


```
./iplistasn.py -d GeoLite2-ASN-CSV_20180403 -c "facebook"
```

Writes the ip list files: `C_FACEBOOK.ip4` and `C_FACEBOOK.ip6`.


## About

Written by Eugen Epure, see the LICENSE file.
