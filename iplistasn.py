#!/usr/bin/env python3

import argparse
import sys
import csv

IPV4_ASNS = "GeoLite2-ASN-Blocks-IPv4.csv"
IPV6_ASNS = "GeoLite2-ASN-Blocks-IPv6.csv"


parser = argparse.ArgumentParser(description=\
            "Generate lists of IP addresses from an GeoLite2 ASN \
                    database in CSV format")

parser.add_argument("-d", "--directory", \
                    help="GeoLite2 ASN directory", required=True)

parser.add_argument("-c", "--companies", \
                    help="companies, comma separated")

parser.add_argument("-n", "--asn", \
                    help="ASN numbers, comma separated")


args = parser.parse_args()


# list filename is ASN_<ASNs>___C_<companies>.ip
list_filename = ''

asn_list = []

if args.asn:
    asn_list = args.asn.split(',')
    if len(asn_list) > 0:
        list_filename = list_filename + 'ASN_' + '_'.join(asn_list)

companies_list = []

if args.companies:    
    companies_list = args.companies.upper().split(',')
    if len(companies_list) > 0:
        if len(list_filename) > 0:
            list_filename = list_filename + '___'
        list_filename = list_filename + 'C_' + '_'.join(companies_list)

list_filename.replace(" ","") # eliminate white spaces


def write_ip_list(asn_csv_filename, ip_list_filename, asns, companies):
    asn_csv_file = open(asn_csv_filename, 'r')
    asn_csv_reader = csv.reader(asn_csv_file, delimiter=',')
    ip_list_file = open(ip_list_filename, 'w')


    for row in asn_csv_reader:
        append_block = False
        if row[1] in asn_list:
            append_block = True

        for c in companies_list:
            if c.upper() in row[2].upper():
                append_block = True

        if append_block:
            print(row[0], file=ip_list_file)

    asn_csv_file.close()
    ip_list_file.close()


# find IPv4 blocks
write_ip_list(args.directory+'/'+IPV4_ASNS, list_filename+'.ip4', \
                asn_list, companies_list)

# find IPv6 blocks
write_ip_list(args.directory+'/'+IPV6_ASNS, list_filename+'.ip6', \
                asn_list, companies_list)
